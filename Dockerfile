FROM python:3.7-alpine
LABEL maintainer="CERN Authoring <authoring@cern.ch>"
LABEL version="1.0.4"
COPY requirements.txt .
RUN pip install -r requirements.txt
